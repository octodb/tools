package io.octodb.test;

import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import org.sqlite.database.sqlite.SQLiteDatabase;
import org.sqlite.database.sqlite.SQLiteStatement;
import org.sqlite.database.sqlite.SQLiteException;

public class MyApplication extends Application {

    SQLiteDatabase db = null;

    MainActivity currentActivity = null;

    @Override
    public void onCreate() {
        super.onCreate();
        ConnectToDb();
    }

    public SQLiteDatabase getConnection() {
        return db;
    }

    public void setCurrentActivity(MainActivity activity) {
        this.currentActivity = activity;
    }

    public void ConnectToDb() {
        try {

            System.loadLibrary("octodb");

            File dbfile = new File(getFilesDir(), "dbtest.db");
            //File dbfile = currentActivity.getApplicationContext().getDatabasePath("dbtest.db");
            String uriStr = "file:" + dbfile.getAbsolutePath() + "?node=secondary&connect=tcp://198.58.100.244:1234";

            Log.d("uri", uriStr);

            db = SQLiteDatabase.openOrCreateDatabase(uriStr, null);

            if (db.isReady()) {
                // the user is logged in. show the main screen
                on_db_is_ready();
            } else {
                // user not logged in. show the signup & login screen
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }

            db.onSync(() -> {
                Log.d("OctoDB Test", "on_db_sync - thread id: " + Thread.currentThread().getId());
                if (currentActivity != null) {
                    currentActivity.onUpdateClick();
                }
            });

        } catch (SQLiteException ex) {
            showMessage("DB open: " + ex.toString());
        } catch (java.lang.Exception ex) {
            showMessage("ConnectToDb: " + ex.toString());
        }
    }

    private void on_db_is_ready() {
        Log.d("OctoDB Test", "the database is ready - thread id: " + Thread.currentThread().getId());
        if (currentActivity != null) {
            currentActivity.on_db_ready();
        }
    }

    /*
    public boolean db_is_ready() {

        try {
            // retrieve the status
            SQLiteStatement statement = db.compileStatement("PRAGMA sync_status");
            String result = statement.simpleQueryForString();
            statement.close();
            // parse the JSON result
            JSONObject jObject = new JSONObject(result);
            return jObject.getBoolean("db_is_ready");
        } catch (SQLiteException ex) {
            showMessage("prepare statement: " + ex.toString());
            db.close();
            return false;
        } catch (java.lang.Exception ex) {
            showMessage("db_is_ready: " + ex.toString());
            return false;
        }

    }
    */

    public void showMessage(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

}
