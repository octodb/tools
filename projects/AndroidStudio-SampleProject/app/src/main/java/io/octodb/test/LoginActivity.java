package io.octodb.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;
import org.sqlite.database.sqlite.SQLiteDatabase;

public class LoginActivity extends AppCompatActivity {

    SQLiteDatabase db = null;
    MyApplication app = null;
    EditText edtEmail, edtPassword;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);

        app = (MyApplication) this.getApplication();
        db = app.getConnection();
    }

    private boolean isValid() {
        String Email = edtEmail.getText().toString().trim();
        String Password = edtPassword.getText().toString().trim();

        if(Email.isEmpty()){
            edtEmail.setError("Please enter Email");
            return false;
        }else if(!Email.matches(emailPattern)){
            edtEmail.setError("Please enter valid Email");
            return false;
        }else if(Password.isEmpty()){
            edtPassword.setError("Please enter Password");
            return false;
        }else if(Password.length() < 6){
            edtPassword.setError("Password must be at least 6 characters");
            return false;
        }else{
            return true;
        }
    }

    public void Login(View view) {
        if (isValid()) {
            try {
                db.execSQL("pragma user_login='" + get_login_info() + "'");
                app.db.onReady(this::on_db_is_ready);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void SignUp(View view) {
        if (isValid()) {
            try {
                db.execSQL("pragma user_signup='" + get_login_info() + "'");
                app.db.onReady(this::on_db_is_ready);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String get_login_info() throws JSONException {
        String email = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        JSONObject json = new JSONObject();
        json.put("email", email);
        json.put("passwd", password);
        String result = json.toString();
        Log.d("OctoDB Test", result);
        return result;  //json.toString();
    }

    private void on_db_is_ready() {
        Log.d("OctoDB Test", "the database is ready - thread id: " + Thread.currentThread().getId());
        Intent i = new Intent(LoginActivity.this,MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        finish();
    }

}
